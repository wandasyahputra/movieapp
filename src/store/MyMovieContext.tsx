import React from 'react';
import { MovieContextType, IMovie } from '../types/Interface';

export const MovieContext = React.createContext<MovieContextType | null>(null);

const MovieProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [movies, setMovie] = React.useState<IMovie[]>(
    localStorage.getItem('movie')
      ? JSON.parse(localStorage.getItem('movie')!)
      : []
  );
  const saveMovie = (newMovie: IMovie) => {
    setMovie([...movies, newMovie]);
    localStorage.setItem('movie', JSON.stringify([...movies, newMovie]));
  };
  const removeMovie = (id: string) => {
    const newMovies = movies.filter((movie: IMovie) => movie.imdbID !== id);
    setMovie([...newMovies]);
    localStorage.setItem('movie', JSON.stringify([...newMovies]));
  };
  return (
    <MovieContext.Provider value={{ movies, saveMovie, removeMovie }}>
      {children}
    </MovieContext.Provider>
  );
};

export default MovieProvider;
