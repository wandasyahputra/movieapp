import { paramsType } from '../types/Interface';

export const generateQueryParam = (param: paramsType) => {
  return new URLSearchParams(param as string[][]);
};

export const fetchApi = async (url: paramsType) => {
  const res = await fetch(
    'http://www.omdbapi.com/?apikey=74a0e1c1&' + generateQueryParam(url)
  );
  const data = await res.json();
  return data;
};

export const elipsis = (str: string, limit: number) => {
  return str.length > limit ? str.substring(0, limit) + '...' : str;
};
