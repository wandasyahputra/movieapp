import React, { useEffect, useState } from 'react';
import { searchMovie } from '../../ApiList';
import { useInfiniteQuery } from '@tanstack/react-query';
import { MovieContext } from '../../store/MyMovieContext';
import { IMovie, MovieContextType } from '../../types/Interface';

const useTableState = () => {
  const [search, setSearch] = useState('');
  const { movies } = React.useContext(MovieContext) as MovieContextType;

  const handleSearch = (val: string) => {
    setSearch(val);
  };

  const {
    data,
    fetchNextPage,
    hasNextPage,
    isFetchingNextPage,
    isFetching,
    isError,
    refetch,
  } = useInfiniteQuery({
    queryKey: ['movieList', search],
    queryFn: async (param) => searchMovie(search, param?.pageParam + 1),
    initialPageParam: 0,
    staleTime: Infinity,
    retry: false,
    getNextPageParam: (lastPage, pages) => {
      console.log(parseInt(lastPage.totalResults) - 10 * pages?.length > 0);
      if (parseInt(lastPage.totalResults) - 10 * pages?.length > 0) {
        return pages.length;
      }
    },
  });

  useEffect(() => {
    const onScroll = () => {
      const movieList = document.getElementById('movieList');
      if (
        (movieList?.scrollHeight || 0) - (window.scrollY + window.innerHeight) <
          300 &&
        hasNextPage &&
        !isFetching &&
        !isFetchingNextPage
      ) {
        fetchNextPage();
      }
    };
    window.addEventListener('scroll', onScroll);

    return () => window.removeEventListener('scroll', onScroll);
  }, [fetchNextPage, hasNextPage, isFetching, isFetchingNextPage]);

  const dataRenderMovieList = () => {
    const renderedData = [] as IMovie[];
    data?.pages.forEach((page) => {
      page.Search?.forEach((movie: IMovie) => {
        if (!movies?.find((saved) => saved.imdbID === movie.imdbID)) {
          renderedData.push(movie);
        }
      });
    });
    return renderedData;
  };

  return {
    search,
    setSearch,
    handleSearch,
    data,
    isError,
    refetch,
    fetchNextPage,
    isFetching,
    hasNextPage,
    dataRenderMovieList,
    isFetchingNextPage,
  };
};
export default useTableState;
