import React, { Fragment } from 'react';
import useTableState from './_hook';
import { Link } from 'react-router-dom';
import Cards from '../../components/Cards';
import Navbar from '../../components/Navbar';

const MovieList = () => {
  const state = useTableState();
  const {
    search,
    handleSearch,
    dataRenderMovieList,
    isFetchingNextPage,
    hasNextPage,
    isFetching,
    isError,
    refetch,
  } = state;
  return (
    <div>
      <Navbar />
      <div className="pl-5 pr-5 pt-3 pb-3">
        <label htmlFor="search">Search</label>
        <input
          id="search"
          className="border rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2 pl-3"
          value={search}
          placeholder="Conan"
          onChange={(e) => handleSearch(e.currentTarget.value)}
        />
      </div>
      <div
        className="flex flex-row flex-wrap pr-3 pl-3 justify-center"
        id="movieList"
      >
        {dataRenderMovieList().map((movie) => (
          <Link
            key={movie.imdbID}
            to={`/movie/${movie.imdbID}`}
            className="inline-block w-1/2 md:w-[350px] pr-2 pl-2 pb-3 m-pointer"
          >
            <Cards data={movie} />
          </Link>
        ))}
        {dataRenderMovieList().length === 0 &&
        search?.length > 2 &&
        !isFetching &&
        !isFetchingNextPage &&
        !isError ? (
          <div className="text-center mt-5 w-full">
            Nothing found, please try with other keyword.
          </div>
        ) : null}
        {dataRenderMovieList().length % 2 !== 0 ? (
          <div className="inline-block w-1/2 md:w-[350px] pr-2 pl-2 pb-3">
            &nbsp;
          </div>
        ) : null}
        {(isFetchingNextPage || isFetching) && search?.length > 2 ? (
          <Fragment>
            <div className="inline-block w-1/2 md:w-[350px] pr-2 pl-2 pb-3 m-pointer">
              <Cards isLoading />
            </div>
            <div className="inline-block w-1/2 md:w-[350px] pr-2 pl-2 pb-3 m-pointer">
              <Cards isLoading />
            </div>
          </Fragment>
        ) : null}
      </div>
      <div className="pr-5 pl-5">
        {isError && (
          <div className="text-center mb-7 flex flex-col">
            <span className="pb-9 pt-9">
              Eww, something went wrong, please try again.
            </span>
            <button
              className="text-white bg-gray-300 rounded-lg block px-5 py-2.5 text-center"
              onClick={() => refetch()}
            >
              Retry
            </button>
          </div>
        )}
      </div>
      {search === '' ? (
        <div className="text-center mt-5">
          Let's Search a Movie that you like!
        </div>
      ) : null}
      {!hasNextPage &&
      search?.length > 2 &&
      !isError &&
      dataRenderMovieList().length !== 0 ? (
        <div className="text-center mb-7">Nothing more to load</div>
      ) : (
        ''
      )}
    </div>
  );
};

export default MovieList;
