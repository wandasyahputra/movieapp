import React, { useCallback, useEffect, useState } from 'react';
import { getMovieDetail } from '../../ApiList';
import { useQueries } from '@tanstack/react-query';
import { IMovie, MovieContextType } from '../../types/Interface';
import { MovieContext } from '../../store/MyMovieContext';

const useTableState = () => {
  const [search, setSearch] = useState('');
  const { movies } = React.useContext(MovieContext) as MovieContextType;
  const [idList, setIdList] = useState<string[]>([]);
  const handleSearch = (val: string) => {
    setSearch(val);
  };

  const pagingMyMovie = useCallback(
    (page: number) => {
      const newIdList = [];
      for (let i = 0; i < movies.length && i < (page + 1) * 10; i++) {
        newIdList.push(movies[i].imdbID);
      }
      return newIdList;
    },
    [movies]
  );

  const handleNextPage = useCallback(() => {
    const nextPage = idList.length / 10;
    if (idList?.length < movies?.length) {
      setIdList(pagingMyMovie(nextPage));
    }
    return;
  }, [idList.length, movies?.length, pagingMyMovie]);

  const hasNextPage = useCallback(() => {
    return idList?.length < movies?.length;
  }, [idList?.length, movies?.length]);

  const filterDataBySearch = () => {
    const data = [] as IMovie[];
    result.forEach((res) => {
      if (res?.data?.Title.toLowerCase().includes(search.toLowerCase())) {
        data.push({
          ...res.data,
          isError: res.isError,
          isLoading: res.isLoading,
        });
      } else if (search === '' && (res.isError || res.isLoading)) {
        data.push({
          ...res.data,
          isError: res.isError,
          isLoading: res.isLoading,
          refetch: res.refetch,
        });
      }
    });
    return data;
  };

  useEffect(() => {
    const onScroll = () => {
      const movieList = document.getElementById('movieList');
      if (
        (movieList?.scrollHeight || 0) - (window.scrollY + window.innerHeight) <
          300 &&
        hasNextPage()
      ) {
        handleNextPage();
      }
    };
    window.addEventListener('scroll', onScroll);

    return () => window.removeEventListener('scroll', onScroll);
  }, [handleNextPage, hasNextPage]);

  useEffect(() => {
    setIdList(pagingMyMovie(0));
  }, [pagingMyMovie]);

  const result = useQueries({
    queries: idList.map((id) => ({
      queryKey: ['movie', id],
      queryFn: async () => getMovieDetail(id),
      staleTime: Infinity,
      retry: false,
    })),
  });

  return {
    search,
    setSearch,
    handleSearch,
    handleNextPage,
    hasNextPage,
    result,
    filterDataBySearch,
  };
};
export default useTableState;
