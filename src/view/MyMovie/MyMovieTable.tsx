import { Link } from 'react-router-dom';
import useTableState from './_hook';
import Navbar from '../../components/Navbar';
import Cards from '../../components/Cards';

const MyMovie = () => {
  const state = useTableState();
  const { search, handleSearch, filterDataBySearch } = state;
  return (
    <div>
      <Navbar title={'My Movie'} />
      <div className="pl-5 pr-5 pt-3 pb-3">
        <label htmlFor="search">Search</label>
        <input
          id="search"
          className="border rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2 pl-3"
          value={search}
          placeholder="Conan"
          onChange={(e) => handleSearch(e.currentTarget.value)}
        />
      </div>
      <div
        className="flex flex-row flex-wrap pr-3 pl-3 justify-center"
        id="movieList"
      >
        {filterDataBySearch()?.map((item, index) =>
          !item.isLoading && !item.isError ? (
            <Link
              key={item?.imdbID || index}
              to={`/movie/${item?.imdbID}`}
              className="inline-block w-1/2 md:w-[350px] pr-2 pl-2 pb-3 m-pointer"
            >
              <Cards data={item} isError={item?.isError} />
            </Link>
          ) : (
            <div className="inline-block w-1/2 md:w-[350px] pr-2 pl-2 pb-3 m-pointer">
              <Cards
                data={item}
                isLoading={item?.isLoading}
                isError={item?.isError}
                refetch={item?.refetch}
              />
            </div>
          )
        )}
        {filterDataBySearch()?.length % 2 !== 0 && (
          <div className="inline-block w-1/2 md:w-[350px] pr-2 pl-2 pb-3">
            &nbsp;
          </div>
        )}
        {filterDataBySearch()?.length === 0 && (
          <div className="text-center mt-5 w-full">
            You didn't save any movie yet. Search Movie and save to you list!
          </div>
        )}
      </div>
    </div>
  );
};

export default MyMovie;
