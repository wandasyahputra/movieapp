import useTableState from './_hook';
import Navbar from '../../components/Navbar';
import { useState } from 'react';

const MovieDetail = () => {
  const tableState = useTableState();
  const [imageError, setImageError] = useState(false);
  const { data, isLoading, saveMovie, removeMovie, isSaved, isError, refetch } =
    tableState;
  return (
    <div>
      <Navbar title="Movie Detail" />
      {isLoading ? (
        <div className="rounded-lg overflow-hidden pt-5 sm:pt-10 animate-pulse sm:flex pr-5 pl-5">
          <div className="flex items-center justify-center rounded-lg w-full sm:inline-flex sm:w-[350px] sm bg-gray-300 sm:h-[550px] h-[650px] dark:bg-gray-700">
            <svg
              className="w-10 h-10 text-gray-200 dark:text-gray-600 rounded-lg"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="currentColor"
              viewBox="0 0 20 18"
            >
              <path d="M18 0H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-5.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3Zm4.376 10.481A1 1 0 0 1 16 15H4a1 1 0 0 1-.895-1.447l3.5-7A1 1 0 0 1 7.468 6a.965.965 0 0 1 .9.5l2.775 4.757 1.546-1.887a1 1 0 0 1 1.618.1l2.541 4a1 1 0 0 1 .028 1.011Z" />
            </svg>
          </div>
          <div className="px-6 py-4 min-h-[148px] inline-block w-full sm:w-[60vw]">
            <div className="font-bold text-xl mb-2">
              <div className="h-5 bg-gray-200 rounded-full dark:bg-gray-400 mb-3"></div>
              <div className="h-5 bg-gray-200 rounded-full dark:bg-gray-300 mb-4 w-3/4"></div>
              <div className="h-5 bg-gray-200 rounded-full dark:bg-gray-400 mb-4 w-1/4"></div>
            </div>
            <div className="space-y-2.5 mt-10">
              <div className="flex items-center w-full">
                <div className="h-5 bg-gray-200 rounded-full dark:bg-gray-700 w-32"></div>
                <div className="h-5 ms-2 bg-gray-300 rounded-full dark:bg-gray-600 w-24"></div>
                <div className="h-5 ms-2 bg-gray-300 rounded-full dark:bg-gray-600 w-full"></div>
              </div>
              <div className="flex items-center w-full max-w-[480px]">
                <div className="h-5 bg-gray-200 rounded-full dark:bg-gray-700 w-full"></div>
                <div className="h-5 ms-2 bg-gray-300 rounded-full dark:bg-gray-600 w-full"></div>
                <div className="h-5 ms-2 bg-gray-300 rounded-full dark:bg-gray-600 w-24"></div>
              </div>
              <div className="flex items-center w-full max-w-[400px]">
                <div className="h-5 bg-gray-300 rounded-full dark:bg-gray-600 w-full"></div>
                <div className="h-5 ms-2 bg-gray-200 rounded-full dark:bg-gray-700 w-80"></div>
                <div className="h-5 ms-2 bg-gray-300 rounded-full dark:bg-gray-600 w-full"></div>
              </div>
              <div className="flex items-center w-full max-w-[480px]">
                <div className="h-5 bg-gray-200 rounded-full dark:bg-gray-700 w-full"></div>
                <div className="h-5 ms-2 bg-gray-300 rounded-full dark:bg-gray-600 w-full"></div>
                <div className="h-5 ms-2 bg-gray-300 rounded-full dark:bg-gray-600 w-24"></div>
              </div>
              <div className="flex items-center w-full max-w-[440px]">
                <div className="h-5 bg-gray-300 rounded-full dark:bg-gray-600 w-32"></div>
                <div className="h-5 ms-2 bg-gray-300 rounded-full dark:bg-gray-600 w-24"></div>
                <div className="h-5 ms-2 bg-gray-200 rounded-full dark:bg-gray-700 w-full"></div>
              </div>
              <div className="flex items-center w-full max-w-[360px]">
                <div className="h-5 bg-gray-300 rounded-full dark:bg-gray-600 w-full"></div>
                <div className="h-5 ms-2 bg-gray-200 rounded-full dark:bg-gray-700 w-80"></div>
                <div className="h-5 ms-2 bg-gray-300 rounded-full dark:bg-gray-600 w-full"></div>
              </div>
            </div>
          </div>
        </div>
      ) : isError ? (
        <div className="flex flex-col align-items-center mt-12 text-center pr-5 pl-5">
          <div>Oooops, can't get your data from server. Please refresh</div>
          <div>
            <button
              className="bg-gray-300 rounded-lg px-5 py-2 text-center mt-12 mb-6"
              onClick={() => refetch()}
            >
              Refresh
            </button>
          </div>
        </div>
      ) : (
        <div className="rounded-lg overflow-hidden pt-5 sm:pt-10 sm:flex pr-5 pl-5">
          <div className="flex items-center justify-center rounded-lg w-full sm:inline-flex sm:w-[350px] sm bg-gray-300 sm:h-[550px] h-[650px] dark:bg-gray-700">
            {imageError ? (
              <div className="flex items-center justify-center w-full bg-gray-300 md:h-[55vh] h-[250px] dark:bg-gray-700">
                <svg
                  className="w-10 h-10 text-gray-200 dark:text-gray-600"
                  aria-hidden="true"
                  xmlns="XXXXXXXXXXXXXXXXXXXXXXXXXX"
                  fill="currentColor"
                  viewBox="0 0 20 18"
                >
                  <path d="M18 0H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-5.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3Zm4.376 10.481A1 1 0 0 1 16 15H4a1 1 0 0 1-.895-1.447l3.5-7A1 1 0 0 1 7.468 6a.965.965 0 0 1 .9.5l2.775 4.757 1.546-1.887a1 1 0 0 1 1.618.1l2.541 4a1 1 0 0 1 .028 1.011Z" />
                </svg>
              </div>
            ) : (
              <img
                className="w-full object-cover sm:h-[550px] h-[650px]  sm:w-[350px] w-full rounded-lg"
                src={data?.Poster}
                onError={() => setImageError(true)}
                alt={data?.Title}
              />
            )}
          </div>
          <div className="px-6 py-4 min-h-[148px] inline-block w-full sm:w-[60vw]">
            <div className="mb-2">
              <h2 className="font-bold text-3xl mb-3">{data?.Title}</h2>
              <p className="mb-4">
                {data?.Rated}, {data?.Runtime}, {data?.Genre}, {data?.Released}
              </p>
              <div className="mb-4">
                {data?.Ratings?.map(
                  (item: { Source: string; Value: string }) => (
                    <div key={item?.Source}>
                      <span>{item?.Source} </span>
                      <span>{item?.Value}</span>,
                    </div>
                  )
                )}
              </div>
              {isSaved(data?.imdbID) ? (
                <button
                  onClick={() => removeMovie(data?.imdbID)}
                  className="bg-gray-300 rounded-lg flex px-5 py-2 text-center mt-4 mb-6"
                >
                  <svg
                    className="w-6 h-6 text-gray-800 mr-4"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="currentColor"
                    viewBox="0 0 14 20"
                  >
                    <path d="M13 20a1 1 0 0 1-.64-.231L7 15.3l-5.36 4.469A1 1 0 0 1 0 19V2a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2v17a1 1 0 0 1-1 1Z"></path>
                  </svg>
                  Remove From My Movie
                </button>
              ) : (
                <button
                  className="bg-gray-300 rounded-lg flex px-5 py-2 text-center mt-4 mb-6"
                  onClick={() => {
                    saveMovie(data);
                  }}
                >
                  <svg
                    className="w-6 h-6 text-gray-800 mr-4"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 20"
                  >
                    <path
                      stroke="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="m13 19-6-5-6 5V2a1 1 0 0 1 1-1h10a1 1 0 0 1 1 1v17Z"
                    ></path>
                  </svg>
                  Save To My Movie
                </button>
              )}
            </div>
            <div className="space-y-2.5 mt-10">{data?.Plot}</div>
          </div>
        </div>
      )}
    </div>
  );
};

export default MovieDetail;
