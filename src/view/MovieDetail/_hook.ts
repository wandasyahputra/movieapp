import React from 'react';
import { getMovieDetail } from '../../ApiList';
import { MovieContext } from '../../store/MyMovieContext';
import { MovieContextType } from '../../types/Interface';
import { useQuery } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

const useTableState = () => {
  const params = useParams();
  const movieDetailQuery = (movieId: string) => ({
    queryKey: ['movie', movieId],
    queryFn: async () => getMovieDetail(movieId),
    refecthOnWindowFocus: false,
    staleTime: Infinity,
    retry: false,
  });
  const { movies, saveMovie, removeMovie } = React.useContext(
    MovieContext
  ) as MovieContextType;

  const result = useQuery({
    queryKey: ['movie', params?.movieId],
    queryFn: async () => getMovieDetail(params?.movieId),
    staleTime: Infinity,
    retry: false,
  });
  const { isLoading, data, isError, refetch } = result;
  const isSaved = (id: string) => {
    return movies?.find((item) => item.imdbID === id);
  };

  return {
    movieDetailQuery,
    saveMovie,
    removeMovie,
    isSaved,
    movies,
    isLoading,
    isError,
    refetch,
    data,
  };
};
export default useTableState;
