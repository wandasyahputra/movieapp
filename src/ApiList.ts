import { fetchApi } from './view/utils';

export const searchMovie = (query: string | undefined, page?: number) => {
  if (!query || query?.length < 3) return [];
  return fetchApi({ s: query, page });
};

export const getMovieDetail = (movieId: string | undefined) => {
  return fetchApi({ i: movieId });
};
