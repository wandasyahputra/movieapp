export interface paramsType {
  i?: string;
  s?: string;
  page?: number;
}

export interface resultType {
  imdbID: string;
  Title: string;
}

export interface IMovie {
  refetch: (() => void) | undefined;
  isLoading: any;
  Title: string;
  Year: string;
  Rated: string;
  Released: string;
  Runtime: string;
  Genre: string;
  Director: string;
  Writer: string;
  Actors: string;
  Plot: string;
  Language: string;
  Country: string;
  Awards: string;
  Poster: string;
  Metascore: string;
  imdbRating: string;
  imdbVotes: string;
  imdbID: string;
  Type: string;
  DVD: string;
  BoxOffice: string;
  Production: string;
  Website: string;
  Response: string;
  isError: boolean;
}

export interface MovieContextType {
  movies: IMovie[];
  saveMovie: (movie: IMovie) => void;
  removeMovie: (id: string) => void;
}
