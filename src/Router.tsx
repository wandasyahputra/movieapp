import { createBrowserRouter } from 'react-router-dom';
import MovieDetail from './view/MovieDetail/MovieDetailTable';
import MovieList from './view/MovieList/MovieListTable';
import MyMovie from './view/MyMovie/MyMovieTable';

const Router = createBrowserRouter([
  {
    children: [
      {
        path: '/',
        element: <MovieList />,
      },
      {
        path: '/movie/:movieId',
        element: <MovieDetail />,
      },
      {
        path: '/my-movie',
        element: <MyMovie />,
      },
    ],
  },
]);

export default Router;
