import { Link, useNavigate } from 'react-router-dom';

const Navbar = (props: { title?: string; isLoading?: boolean }) => {
  const { title, isLoading } = props;
  const navigate = useNavigate();
  if (title) {
    return (
      <nav className="w-full h-[60px]">
        <div className="w-full fixed py-4 shadow-lg bg-white">
          <div className="flex w-full">
            <div className="pl-4">
              <svg
                className="w-6 h-6 text-gray-800 m-pointer"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                onClick={() => navigate(-1)}
                viewBox="0 0 8 14"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M7 1 1.3 6.326a.91.91 0 0 0 0 1.348L7 13"
                />
              </svg>
            </div>
            <h1 className="text-center text-xl font-bold flex-grow">
              {isLoading ? (
                <div className="h-5 bg-gray-200 rounded-full dark:bg-gray-400 mb-3"></div>
              ) : (
                title
              )}
            </h1>
            <div className="w-[44px]">&nbsp;</div>
          </div>
        </div>
      </nav>
    );
  }
  return (
    <nav className="w-full h-[60px]">
      <div className="w-full fixed py-4 shadow-lg bg-white">
        <div className="flex w-full">
          <div className="w-[44px]">&nbsp;</div>
          <h1 className="text-center text-xl font-bold flex-grow">
            Movie Library
          </h1>
          <Link to="/my-movie" className="pr-5" title="My Movie">
            <svg
              className="w-6 h-6 text-gray-800 "
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="currentColor"
              viewBox="0 0 14 20"
            >
              <path d="M13 20a1 1 0 0 1-.64-.231L7 15.3l-5.36 4.469A1 1 0 0 1 0 19V2a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2v17a1 1 0 0 1-1 1Z"></path>
            </svg>
          </Link>
        </div>
      </div>
    </nav>
  );
};
export default Navbar;
