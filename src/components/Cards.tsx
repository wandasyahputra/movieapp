import { useEffect, useState } from 'react';
import { IMovie } from '../types/Interface';
import { elipsis } from '../view/utils';

const Cards = (props: {
  data?: IMovie;
  isLoading?: Boolean;
  isError?: Boolean;
  refetch?: () => void;
}) => {
  const { data, isLoading, isError, refetch } = props;
  const [imageError, setImageError] = useState(false);

  useEffect(() => {
    if (isError) {
      setImageError(true);
    }
  }, [isError]);
  if (isLoading)
    return (
      <div className="rounded-lg overflow-hidden shadow-lg animate-pulse">
        <div className="flex items-center justify-center w-full bg-gray-300 md:h-[55vh] h-[250px] dark:bg-gray-700">
          <svg
            className="w-10 h-10 text-gray-200 dark:text-gray-600"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="currentColor"
            viewBox="0 0 20 18"
          >
            <path d="M18 0H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-5.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3Zm4.376 10.481A1 1 0 0 1 16 15H4a1 1 0 0 1-.895-1.447l3.5-7A1 1 0 0 1 7.468 6a.965.965 0 0 1 .9.5l2.775 4.757 1.546-1.887a1 1 0 0 1 1.618.1l2.541 4a1 1 0 0 1 .028 1.011Z" />
          </svg>
        </div>
        <div className="px-6 py-4 min-h-[148px]">
          <div className="font-bold text-xl mb-2">
            <div className="h-5 bg-gray-200 rounded-full dark:bg-gray-400 mb-3"></div>
            <div className="h-5 bg-gray-200 rounded-full dark:bg-gray-300 mb-4 w-3/4"></div>
            <div className="h-5 bg-gray-200 rounded-full dark:bg-gray-400 mb-4 w-1/4"></div>
          </div>
          <p className="text-gray-700 text-base">&nbsp;</p>
        </div>
        <div className="px-6 pt-4 pb-2">
          <span className="inline-block w-[70px] bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
            &nbsp;
          </span>
        </div>
      </div>
    );
  return (
    <div className="rounded-lg overflow-hidden shadow-lg">
      {imageError ? (
        <div className="flex items-center justify-center w-full bg-gray-300 md:h-[55vh] h-[250px] dark:bg-gray-700">
          <svg
            className="w-10 h-10 text-gray-200 dark:text-gray-600"
            aria-hidden="true"
            xmlns="XXXXXXXXXXXXXXXXXXXXXXXXXX"
            fill="currentColor"
            viewBox="0 0 20 18"
          >
            <path d="M18 0H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-5.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3Zm4.376 10.481A1 1 0 0 1 16 15H4a1 1 0 0 1-.895-1.447l3.5-7A1 1 0 0 1 7.468 6a.965.965 0 0 1 .9.5l2.775 4.757 1.546-1.887a1 1 0 0 1 1.618.1l2.541 4a1 1 0 0 1 .028 1.011Z" />
          </svg>
        </div>
      ) : (
        <img
          className="w-full object-cover md:h-[55vh] h-[250px]"
          src={data?.Poster}
          onError={() => setImageError(true)}
          alt={data?.Title}
        />
      )}
      <div className="px-6 py-4 min-h-[148px]">
        {!isError ? (
          <>
            <div className="font-bold text-xl mb-2">
              {elipsis(data?.Title || '', 30)}
            </div>
            <p className="text-gray-700 text-base">{data?.Year}</p>
          </>
        ) : (
          <div className="flex flex-col align-items-center">
            <span>Ooops, can't get your data from server. Please refresh</span>
            <button
              onClick={refetch}
              className="text-white bg-gray-300 rounded-lg block px-5 py-2 text-center mt-4 mb-6"
            >
              Refresh
            </button>
          </div>
        )}
      </div>
      <div className="px-6 pt-4 pb-2">
        {!isError ? (
          <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
            {data?.Type ? `#${data?.Type}` : ''}
          </span>
        ) : (
          <div>
            <div>&nbsp;</div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Cards;
