This is an Movie website

Demo of this project can be found on [http://mymovie-build.s3-website.eu-central-1.amazonaws.com/](http://mymovie-build.s3-website.eu-central-1.amazonaws.com/)

## Getting Started

This app tested on node.js v18.17.0

First, install the dependencies:

```bash
npm install
```

Then, run dev server:

```bash
npm start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Authors

- **Wanda Syahputra** - [wandasyahputra](https://github.com/wandasyahputra)
